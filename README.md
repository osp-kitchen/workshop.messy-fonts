Workshop on Variable Fonts and manual interpolation (with stroke fonts inside) with La Cambre master students in type media, Brussels, 2018

Second workshop ‘Messy Fonts’ with La Cambre master students in type media, Brussels, 2021

## Cloning
First clone this repository. When you have no account on the Constant gitlab –**you'll not be able to push**– use:
```
git clone http://gitlab.constantvzw.org/osp/workshop.dirty-variables.git
```

If you have an account use:
```
git clone git@gitlab.constantvzw.org:osp/workshop.dirty-variables.git
```

Some example fonts were added as a submodule. It is possible to retrieve them with the following commands:
```
git submodule init
git submodule update 
```

## Process

To edit Variable fonts without a Graphical User Interface we use the ttx format; an XML dump of a truetype font (ttf). 

It is essentially an XML representation of the font and all its tables. As it's also plain text we can (easily) modify it with a code editor. 

To add and edit variations we need to modify the `avar`, `fvar` and `gvar` tables. In the folder `utilities` you'll find scripts to help with that.


## Conversion to and from ttx

Conversion between ttf ttx is done using [fonttools](https://github.com/fonttools/fonttools), a python based library for modifying fonts.

To convert from ttf to ttx run:
```
ttx name-of-the-font.ttf
```

To convert from ttx to ttf run:
```
ttx name-of-the-font.ttx
```

## Utilities
- `preview-template.html` html interface to preview / visualize a variable font. For printing the variation it offers the option to generate ‘frames’: positions on the variation axis.
- `make-gvar-table.py` adds an `fvar` and `gvar` table to a non-variable truetype font. The `gvar` table will be based on the points in the `glyf` table.
- `modify-gvar-table.py` programmatically modifies the delta's of a variable font.
- `insert-svgs.py` adds an SVG table to the given font based on a folder with SVG files.
- `merge-svgs.py` a script to merge / overlay a folder of svg files into one.
- `inspector/index.html` visualizes the glyph-contours and delta's within a variable font.


### make-gvar-table.py

Adds a `fvar` and `gvar` table to a ttx, based on the existing `glyf` table. Call with either one argument, the path to the ttx that will be updated. Or two, the path to the ttx file to read, and the path to write.

```
make-gvar-table.py path-in.ttx [path-out.ttx]
```

### modify-gvar-table.py

Modifies the `gvar` table in a ttx. Call with either one argument: the path to the ttx that will be updated. Or two: the path to the ttx file to read and the path to write.
```
modify-gvar-table.py path-in.ttx [path-out.ttx]
```

### insert-svgs.py

Inserts a collection of SVG's into the SVG table of a font. If the font doesn't have an SVG table yet it will be added. The SVG's must have the same glyphName (as encoded in the font) as the glyph you want it to replace. `A.svg` → `A`, `zero.svg` → `zero`.

The script is called with three arguments, the filename of the input ttx, the path to the folder containing the SVG's, the output ttx. 

```
insert-svgs.py fontname-in.ttx foldernameofsvgs fontname-out.ttx
```

### merge-svgs.py

Merges svgs within a folder into one drawing. Call with two arguments. The folder containing the SVG's and the the output path for the merged drawing.
```
merge-svgs.py folder out.svg
```