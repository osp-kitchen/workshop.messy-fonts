import lxml.etree as et

fvarTemplate = """<fvar>
  <!-- Weight -->
  <Axis>
    <AxisTag>MVMT</AxisTag>
    <Flags>0x0</Flags>
    <MinValue>0.0</MinValue>
    <DefaultValue>0.0</DefaultValue>
    <MaxValue>100.0</MaxValue>
    <AxisNameID>256</AxisNameID>
  </Axis>
</fvar>"""

gvarTemplate = """<gvar>
    <version value="1"/>
    <reserved value="0"/>
</gvar>"""

"""
  Generates a fvartable based on a template with the given
  minValue, maxValue and defaultValue
"""
def makeFvarTable (tag, minValue, maxValue, defaultValue, nameID):
  table = et.fromstring(fvarTemplate, et.XMLParser(remove_blank_text=True))

  axisTag = table.find('Axis/AxisTag')
  axisTag.text = tag
  
  axisMinValue = table.find('Axis/MinValue')
  axisMinValue.text = '{:-.1f}'.format(minValue)

  axisMaxValue = table.find('Axis/MaxValue')
  axisMaxValue.text = '{:-.1f}'.format(maxValue)

  axisDefaultValue = table.find('Axis/DefaultValue')
  axisDefaultValue.text = '{:-.1f}'.format(defaultValue)

  axisNameID = table.find('Axis/AxisNameID')
  axisNameID.text = '{}'.format(nameID)

  return table

"""
 Return glyph with given name
"""
def findGlyph (ttx, glyphName):
  return ttx.find("glyf/TTGlyph[@name='{}']".format(glyphName))

"""
 Return glyph with given name
"""
def findAllGlyphs (ttx):
  return ttx.findall("glyf/TTGlyph")

def getGlyphPoints (glyph):
  return glyph.findall("contour/pt")

def makeGvarTable (ttx, axisTag):
  # gvar = et.SubElement(ttx.getroot(), 'gvar')
  # version = et.SubElement(gvar, 'version', { 'value': str(1) })
  # reserved = et.SubElement(gvar, 'reserved', { 'value': str(0) })
  gvar = et.fromstring(gvarTemplate, et.XMLParser(remove_blank_text=True))

  for glyph in findAllGlyphs(ttx):
    glyphVariations = et.SubElement(gvar, 'glyphVariations', { 'glyph': glyph.get('name') })
    glyphTuple = et.SubElement(glyphVariations, 'tuple')
    coord = et.SubElement(glyphTuple, 'coord', { 'axis': axisTag, 'value': str(1) })

    for pointNumber, point in enumerate(getGlyphPoints(glyph)):
      et.SubElement(glyphTuple, 'delta', { 'pt': str(pointNumber), 'x': str(0), 'y': str(0) })

  return gvar

def addGvarTable (ttx, axisTag, minValue, maxValue, defaultValue, axisID=256):
  if ttx.find('fvar') is not None:
    print('Font already has a table \'fvar\'')
  else:
    fvar = makeFvarTable(axisTag, minValue, maxValue, defaultValue, axisID)
    ttx.getroot().append(fvar)

  if ttx.find('gvar') is not None:
    print('Font already has a table \'gvar\'')
  else:
    gvar = makeGvarTable(ttx, axisTag)
    ttx.getroot().append(gvar)
  
if __name__ == '__main__':
  import sys

  if len(sys.argv) < 3:
    print("Usage: make-gvar-table.py in.ttx out.ttx")

  pathIn = sys.argv[1]
  pathOut = sys.argv[2]

  with open(pathIn, 'r') as h:
    parser = et.XMLParser(remove_blank_text=True)
    ttx = et.parse(h, parser=parser)
    axisName = 'MVMT'
    minValue = 0
    maxValue = 100
    defaultValue = 20
    addGvarTable(ttx, 'MVMT', minValue, maxValue, defaultValue, 256)

    ttx.write(pathOut, pretty_print=True, doctype='<?xml version="1.0" encoding="UTF-8"?>')
