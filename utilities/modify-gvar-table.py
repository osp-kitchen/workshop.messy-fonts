import lxml.etree as et

fvarTemplate = """<fvar>
  <!-- Weight -->
  <Axis>
    <AxisTag>MVMT</AxisTag>
    <Flags>0x0</Flags>
    <MinValue>0.0</MinValue>
    <DefaultValue>0.0</DefaultValue>
    <MaxValue>100.0</MaxValue>
    <AxisNameID>256</AxisNameID>
  </Axis>
</fvar>"""

gvarTemplate = """<gvar>
    <version value="1"/>
    <reserved value="0"/>
</gvar>"""

"""
  Generates a fvartable based on a template with the given
  minValue, maxValue and defaultValue
"""
def makeFvarTable (tag, minValue, maxValue, defaultValue, nameID):
  table = et.fromstring(fvarTemplate)
  
  print(table, table.find('Axis/MaxValue'))

  axisTag = table.find('Axis/AxisTag')
  axisTag.text = tag

  print('{:-.1f}'.format(minValue))

  axisMinValue = table.find('Axis/MinValue')
  axisMinValue.text = '{:-.1f}'.format(minValue)

  axisMaxValue = table.find('Axis/MaxValue')
  axisMaxValue.text = '{:-.1f}'.format(maxValue)

  axisDefaultValue = table.find('Axis/DefaultValue')
  axisDefaultValue.text = '{:-.1f}'.format(defaultValue)

  axisNameID = table.find('Axis/AxisNameID')
  axisNameID.text = '{}'.format(nameID)

  return table

"""
 Return glyph with given name
"""
def findGlyph (ttx, glyphName):
  return ttx.find("glyf/TTGlyph[@name='{}']".format(glyphName))

"""
 Return glyph with given name
"""
def findAllGlyphs (ttx):
  return ttx.findall("glyf/TTGlyph")

def getGlyphPoints (glyph):
  return glyph.findall("contour/pt")

def getGvarTable (ttx):
  return ttx.find('gvar')

def getGvarVariations (ttx):
    gvar = getGvarTable(ttx)

    for glyphVariation in gvar.findall("glyphVariations"):
      glyphName = glyphVariation.get('glyph')
      glyph = findGlyph(ttx, glyphName)
      glyphPoints = getGlyphPoints(glyph)
      deltas =  glyphVariation.findall('tuple/delta')

      yield (
        glyphName,
        glyph,
        glyphPoints,
        deltas
      )


def getPoint(delta):
  return int(delta.get('pt'))

def setPoint(delta, point):
  return delta.set('pt', str(point))

def getX(delta):
  return int(delta.get('x'))

def setX(delta, x):
  return delta.set('x', str(x))

def getY(delta):
  return int(delta.get('y'))

def setY(delta, x):
  return delta.set('y', str(x))

if __name__ == '__main__':
  import sys

  if len(sys.argv) < 3:
    print("Usage: make-gvar-table.py in.ttx out.ttx")

  pathIn = sys.argv[1]
  pathOut = sys.argv[2]

  with open(pathIn, 'r') as h:
    ttx = et.parse(h, et.XMLParser(remove_blank_text=True))
    
    for glyphName, glyph, glyphPoints, deltas in getGvarVariations(ttx):
      for cnt, delta in enumerate(deltas):
        # Pure horizontal shift
        setX(delta, getX(delta) + 800)
        
        # More shift in later points
        # setX(delta, getX(delta) + cnt * 50)
        # setY(delta, getY(delta) + cnt * 30)


    
    ttx.write(pathOut, pretty_print=True, doctype='<?xml version="1.0" encoding="UTF-8"?>')