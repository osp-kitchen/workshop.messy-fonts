# Overlays a set of svg files

# Loop through all glyphs in folder
# Open SVG
# Extract viewbox
#   get character height
#   get character width
  
# Set scale based on desired row height

# Make a group, insert elements of the glyph in it.
# Add group to output drawing
# Scale & translate group to correct position

import lxml.etree as et
import glob
import os
import os.path
import sys
import math

inputFolder = os.path.join(os.getcwd(), sys.argv[1]) if len(sys.argv) > 1 else None
outputFile = sys.argv[2] if len(sys.argv) > 2 else None

if not (inputFolder and outputFile and os.path.isdir(inputFolder)):
  print("Usage: merge-svgs.py inputFolder outputfile")
  exit(1)

outputTemplate = """<?xml version="1.0"?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
</svg>"""

outputRoot = et.fromstring(outputTemplate)
output = et.ElementTree(outputRoot)

# Tansform filenames back to integer again and sort
sortedFiles = sorted(os.listdir(inputFolder), key=lambda f: int(os.path.splitext(f)[0]))

xMin = yMin = math.inf
xMax = yMax = -math.inf

for idx, svgFile in enumerate(sortedFiles):
  with open(os.path.join(inputFolder, svgFile), 'r') as h:
    drawing = et.parse(h)
    root = drawing.getroot()

    if 'viewBox' in root.attrib:
      viewBox = root.attrib['viewBox']
      xMinFile, yMinFile, xMaxFile, yMaxFile = map(float, viewBox.split(' '))

      xMin = min(xMin, xMinFile)
      yMin = min(yMin, yMinFile)
      xMax = max(xMax, xMaxFile)
      yMax = max(yMax, yMaxFile)

    # Try to find glyps
    symbols = drawing.findall('//{http://www.w3.org/2000/svg}symbol')
    for symbol in symbols:
      oldId = symbol.get('id')
      if oldId.startswith('glyph'):
        newId = 'glyph-{}-{}'.format(idx, oldId[5:]) # starts with glyph, which we chop off
        symbol.set('id', newId)
        links = drawing.findall('//{{http://www.w3.org/2000/svg}}use[@{{http://www.w3.org/1999/xlink}}href="#{}"]'.format(oldId))
        for link in links:
          link.set('{http://www.w3.org/1999/xlink}href', '#{}'.format(newId))


    # Try to find the background
    rects = drawing.findall('//{http://www.w3.org/2000/svg}rect')
    for rect in rects:
      if int(rect.get('x')) == xMin \
        and int(rect.get('y')) == yMin \
        and int(rect.get('width')) == xMax \
        and int(rect.get('height')) == yMax:
        rect.getparent().remove(rect)

    # Remove elements with a white fill.
    white = drawing.xpath('//*[contains(@style, "fill:rgb(100%,100%,100%)")]')
    for element in white:
      element.getparent().remove(element)

    g = et.SubElement(outputRoot, 'g')
    g.set('id', svgFile)
    g.set('{http://www.inkscape.org/namespaces/inkscape}groupmode', 'layer')
    g.extend(root.iterchildren())

outputRoot.attrib['width'] = str(xMax - xMin)
outputRoot.attrib['height'] = str(yMax - yMin)
outputRoot.attrib['viewBox'] = '{} {} {} {}'.format(xMin, yMin, xMax, yMax)

output.write(outputFile)