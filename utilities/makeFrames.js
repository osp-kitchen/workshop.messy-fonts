(function () {
  'use strict';

  function makeFrames (variation, selector, amount, start, stop) {
    var frame = document.querySelector(selector),
        step = (stop - start) / (amount - 1);

    if (start === 0) {
      // In firefox a variation at 0 seems encoded as a font rather than the shapes
      // for us it's better to have the shapes. Therefor, force a variation.
      start = 0.1;
    }

    frame.style.fontVariationSettings = '"' + variation + '" ' + start; 

    for (var i = 1; i < amount; i++) {
      copy = frame.cloneNode(true);
      copy.style.fontVariationSettings = '"' + variation + '" ' + (start + i * step); 
      frame.parentElement.appendChild(copy);
    }

  }

  window.makeFrames = makeFrames;

})();